import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JTabbedPane tabbedPane1;
    private JPanel root;
    private JButton ramenButton;
    private JButton gyozaButton;
    private JButton karaageButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton tenpuraButton;
    private JButton waterButton;
    private JButton teaButton;
    private JButton colaButton;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JButton orderCancelButton;
    private JLabel totalAmountLabel;
    private JButton sodaButton;

    public FoodOrderingMachine() {
        orderCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int  confirmation =JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel your order?",
                        "cancel confirm",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null, "canceled your order");
                    String currentText = orderedItemsList.getText();
                    orderedItemsList.setText("");
                    totalAmountLabel.setText("0");
                }

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int  confirmation =JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout？",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + totalAmountLabel.getText() + " yen.");
                    String currentText = orderedItemsList.getText();
                    orderedItemsList.setText("");
                    totalAmountLabel.setText("0");
                }
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen_syouyu.png")));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 300);

            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("karaagekun.png")));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 250);

            }
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("food_gyouza_mise.png")));
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 200);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("food_udon.png")));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 350);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("omatsuri_yakisoba.png")));
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba",310);
            }
        });
        tenpuraButton.setIcon(new ImageIcon(this.getClass().getResource("food_tenpura.png")));
        tenpuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tenpura", 280);
            }
        });
        waterButton.setIcon(new ImageIcon(this.getClass().getResource("medicine_cup_water.png")));
        waterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Water", 0);
            }
        });
        teaButton.setIcon(new ImageIcon(this.getClass().getResource("petbottle_tea.png")));
        teaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tea", 100);
            }
        });
        colaButton.setIcon(new ImageIcon(this.getClass().getResource("drink_cola_petbottle.png")));
        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cola", 150);
            }
        });
        sodaButton.setIcon(new ImageIcon(this.getClass().getResource("juice_soda.png")));
        sodaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soda", 120);
            }
        });
    }
    void order(String food, int foodPrice){
        int  confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "？",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + " " + foodPrice + " yen" + "\n");
            int x;
            String currentValue = totalAmountLabel.getText();
            x = foodPrice + Integer.parseInt(currentValue);
            String totalAmount = String.valueOf(x);
            totalAmountLabel.setText(totalAmount);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        GraphicsDevice gd = gs[0];
        GraphicsConfiguration[] gc = gd.getConfigurations();
        GraphicsConfiguration gc0 = gc[0];
        Rectangle rect = gc0.getBounds();
        int dw = rect.width;
        int dh = rect.height;
        int ww = 300;
        int wh = 200;
        Rectangle rct = new Rectangle( dw, dh);
        frame.setBounds(rct);
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Food Ordering Machine");
        frame.setVisible(true);
    }
}
